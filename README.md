# XML Parser

## About

These example of xml reader in Golang with the feature of http response, kafka, io, testing, mongodb and deployment. 

## Execute
```Shell
make run
```
```
http://localhost:3000
```

## Testing

```Shell
make test
```

```Shell
make bench
```

## Build
```Shell
make build
```