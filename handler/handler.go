package handler

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"xmlparser/models"
	"xmlparser/utils"
)

//ParseMerchantHandler : function for parse merchant response xml
func ParseMerchantHandler(w http.ResponseWriter, r *http.Request) {
	filepath := "assets/merchant.xml"
	xmlFiles, _ := utils.ReadXML(filepath)

	//Transform to struct
	xmlMerchant, err := utils.ParserMerchantResponse(xmlFiles)
	if err != nil {
		log.Fatalf("Merchant Parser: %s", err)
	}

	b, err := json.Marshal(TransformResponse(xmlMerchant))
	if err != nil {
		fmt.Println(err)
		return
	}

	w.Header().Set("Content-Type", "text/html;charset=UTF-8")
	fmt.Fprintf(w, string(b))
}

//TransformResponse : function to transform response
func TransformResponse(resp models.MerchantResponseData) models.SuccessResponse {
	return models.SuccessResponse{
		Data: fmt.Sprintf("%s, %s", resp.ErrorCode, resp.AuthToken),
	}
}
