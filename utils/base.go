package utils

import (
	"encoding/json"
	"encoding/xml"
	"xmlparser/models"
)

//ParseJSONToXML : parse json to xml
func ParseJSONToXML() string {
	var xmlResp models.MerchantResponseData

	jsonStr := `{"ErrorCode":"test","CustomerID":"ABC1234"}`
	json.Unmarshal([]byte(jsonStr), &xmlResp)
	out, _ := xml.MarshalIndent(xmlResp, "\t", "\t")

	return string(out)
}
