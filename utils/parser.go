package utils

import (
	"encoding/xml"
	"os"
	"xmlparser/models"
)

//ParserMerchantResponse : MerchantResponse parser
func ParserMerchantResponse(file *os.File) (models.MerchantResponseData, error) {
	var mc models.MerchantResponseData
	if err := xml.NewDecoder(file).Decode(&mc); err != nil {
		return models.MerchantResponseData{}, err
	}
	defer file.Close()

	return mc, nil
}
