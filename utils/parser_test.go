package utils

import (
	"fmt"
	"testing"
	"xmlparser/models"
)

func TestParserMerchantResponseSuccess(t *testing.T) {
	filepath := "../test/assets/test.xml"
	xmlFiles, _ := ReadXML(filepath)
	res, err := ParserMerchantResponse(xmlFiles)
	expect := models.MerchantResponseData{
		ErrorCode:       "NoError",
		CustomerID:      "22346354",
		AuthToken:       "ABC1234",
		Balance:         0,
		OpenBetsBalance: "0",
		TransactionID:   "-1",
	}
	if err != nil {
		t.Error("TestParserMerchantResponseSuccess should return response")
	}
	if res != expect {
		t.Error(fmt.Sprintf("TestParserMerchantResponseSuccess should return %v got %v", expect, res))
	}
}

func TestParserMerchantResponseError(t *testing.T) {
	filepath := "../test/assets/testfail.xml"
	xmlFiles, _ := ReadXML(filepath)
	_, err := ParserMerchantResponse(xmlFiles)

	expect := `strconv.ParseFloat: parsing "Zero": invalid syntax`
	if err.Error() != expect {
		t.Error(fmt.Sprintf("TestParserMerchantResponseSuccess should %v got %v", expect, err.Error()))
	}
}

func BenchmarkTestParserMerchantResponse(b *testing.B) {
	// run function b.N times
	for n := 0; n < b.N; n++ {
		filepath := "../test/assets/test.xml"
		xmlFiles, _ := ReadXML(filepath)
		ParserMerchantResponse(xmlFiles)
	}
}
