package main

import (
	"fmt"
	"net/http"
	"xmlparser/handler"
	"xmlparser/utils"
)

func main() {
	fmt.Println("Parser start...")
	utils.ParseJSONToXML()
	http.HandleFunc("/", handler.ParseMerchantHandler)
	http.ListenAndServe(":3000", nil)
}
